<!-- TOC -->

- [Mục tiêu của đề tài](#m%E1%BB%A5c-ti%C3%AAu-c%E1%BB%A7a-%C4%91%E1%BB%81-t%C3%A0i)
  - [Mục tiêu chung](#m%E1%BB%A5c-ti%C3%AAu-chung)
  - [Mục tiêu cụ thể](#m%E1%BB%A5c-ti%C3%AAu-c%E1%BB%A5-th%E1%BB%83)
- [Tổng quan tình hình nghiên cứu, luận giải về mục tiêu và những nội dung nghiên cứu của đề tài](#t%E1%BB%95ng-quan-t%C3%ACnh-h%C3%ACnh-nghi%C3%AAn-c%E1%BB%A9u-lu%E1%BA%ADn-gi%E1%BA%A3i-v%E1%BB%81-m%E1%BB%A5c-ti%C3%AAu-v%C3%A0-nh%E1%BB%AFng-n%E1%BB%99i-dung-nghi%C3%AAn-c%E1%BB%A9u-c%E1%BB%A7a-%C4%91%E1%BB%81-t%C3%A0i)
  - [Đánh giá tổng quan tình hình nghiên cứu thuộc lĩnh vực của đề tài](#%C4%91%C3%A1nh-gi%C3%A1-t%E1%BB%95ng-quan-t%C3%ACnh-h%C3%ACnh-nghi%C3%AAn-c%E1%BB%A9u-thu%E1%BB%99c-l%C4%A9nh-v%E1%BB%B1c-c%E1%BB%A7a-%C4%91%E1%BB%81-t%C3%A0i)
    - [Về dữ liệu](#v%E1%BB%81-d%E1%BB%AF-li%E1%BB%87u)
      - [Ngoài nước](#ngo%C3%A0i-n%C6%B0%E1%BB%9Bc)
      - [Trong nước](#trong-n%C6%B0%E1%BB%9Bc)
    - [Về công nghệ](#v%E1%BB%81-c%C3%B4ng-ngh%E1%BB%87)
    - [XVector](#xvector)
      - [ResNet](#resnet)
      - [ECAPA-TDNN](#ecapa-tdnn)
      - [MFA Conformer](#mfa-conformer)
  - [Luận giải về việc đặt ra mục tiêu, đối tượng và những nội dung cần nghiên cứu của đề tài](#lu%E1%BA%ADn-gi%E1%BA%A3i-v%E1%BB%81-vi%E1%BB%87c-%C4%91%E1%BA%B7t-ra-m%E1%BB%A5c-ti%C3%AAu-%C4%91%E1%BB%91i-t%C6%B0%E1%BB%A3ng-v%C3%A0-nh%E1%BB%AFng-n%E1%BB%99i-dung-c%E1%BA%A7n-nghi%C3%AAn-c%E1%BB%A9u-c%E1%BB%A7a-%C4%91%E1%BB%81-t%C3%A0i)
    - [Luận giải mục tiêu, đối tượng của đề tài](#lu%E1%BA%ADn-gi%E1%BA%A3i-m%E1%BB%A5c-ti%C3%AAu-%C4%91%E1%BB%91i-t%C6%B0%E1%BB%A3ng-c%E1%BB%A7a-%C4%91%E1%BB%81-t%C3%A0i)
    - [Những nội dung cần nghiên cứu:](#nh%E1%BB%AFng-n%E1%BB%99i-dung-c%E1%BA%A7n-nghi%C3%AAn-c%E1%BB%A9u)
      - [Xây dựng bộ dữ liệu huấn luyện](#x%C3%A2y-d%E1%BB%B1ng-b%E1%BB%99-d%E1%BB%AF-li%E1%BB%87u-hu%E1%BA%A5n-luy%E1%BB%87n)
        - [Mô tả](#m%C3%B4-t%E1%BA%A3)
        - [Phương pháp xây dựng](#ph%C6%B0%C6%A1ng-ph%C3%A1p-x%C3%A2y-d%E1%BB%B1ng)
      - [Xây dựng mô hình học sâu](#x%C3%A2y-d%E1%BB%B1ng-m%C3%B4-h%C3%ACnh-h%E1%BB%8Dc-s%C3%A2u)
        - [Mô tả](#m%C3%B4-t%E1%BA%A3)
        - [Phương pháp xây dựng](#ph%C6%B0%C6%A1ng-ph%C3%A1p-x%C3%A2y-d%E1%BB%B1ng)
- [Sản phẩm KHCN chính của đề tài và yêu cầu chất lượng cần đạt](#s%E1%BA%A3n-ph%E1%BA%A9m-khcn-ch%C3%ADnh-c%E1%BB%A7a-%C4%91%E1%BB%81-t%C3%A0i-v%C3%A0-y%C3%AAu-c%E1%BA%A7u-ch%E1%BA%A5t-l%C6%B0%E1%BB%A3ng-c%E1%BA%A7n-%C4%91%E1%BA%A1t)

<!-- /TOC -->

# Mục tiêu của đề tài

## Mục tiêu chung

Xây dựng bộ dữ liệu chất lượng cao về nhận dạng người nói tiếng Việt. Bộ dữ liệu này là thành phần quan trọng để huấn luyện và kiểm thử hệ thống nhận diện người nói tiếng Việt, góp phần xác minh danh tính trong giao dịch ngân hàng, cải thiện an ninh trong các hbệ thống bảo mật, kiểm soát truy cập vào các thiết bị điện tử và sử dụng trong trợ lý ảo.

## Mục tiêu cụ thể

Bộ dữ liệu được xây dựng cần đáp ứng các yêu cầu sau đây:

- *Quy mô lớn*: Xác định số lượng người tham gia (5000 người) và thời lượng ghi âm của mỗi người (lớn hơn 20 phút) sao cho đủ lớn để đảm bảo tính đại diện cho toàn bộ dân số muốn huấn luyện.
- *Tính đa dạng*: Nên chọn những người nói đến từ nhiều địa phương, tầng lớp, trình độ, độ tuổi, phong cách nói giới tính và trình độ khác nhau để đảm bảo tính đa dạng và đại diện cho đa số người Việt Nam. Cũng cần đảm bảo sự phân phối đồng đều các đối tượng người nói trong bộ dữ liệu huấn luyện. Điều này sẽ giúp đảm bảo rằng mô hình học được một lượng đủ lớn thông tin về mỗi người nói, tránh được trường hợp một số người nói được sử dụng quá nhiều trong bộ dữ liệu huấn luyện, gây ra hiện tượng quá khớp (overfitting).
- *Chất lượng cao*: Sử dụng thiết bị ghi âm chất lượng tốt để thu âm các bản ghi. Đồng thời, yêu cầu người nói phát âm rõ ràng và tự nhiên. Bên cạnh đó, cần bảo các bản ghi âm đều được thu trong các điều kiện giống nhau như âm lượng, tần số, môi trường... để đảm bảo tính nhất quán của dữ liệu.
- *Tính hợp pháp*: Đảm bảo việc thu thập dữ liệu được thực hiện hợp pháp, bảo vệ quyền riêng tư và đáp ứng các quy định pháp luật về bảo vệ dữ liệu cá nhân.

# Tổng quan tình hình nghiên cứu, luận giải về mục tiêu và những nội dung nghiên cứu của đề tài

## Đánh giá tổng quan tình hình nghiên cứu thuộc lĩnh vực của đề tài

Trong thế giới ngày nay, công nghệ nhận dạng người nói đang được sử dụng trong nhiều lĩnh vực, từ bảo mật, an ninh đến điều khiển giọng nói và giao tiếp trợ lý ảo. Trong lĩnh vực an ninh, công nghệ nhận dạng người nói được sử dụng để phát hiện và giám sát các hoạt động tội phạm trên điện thoại di động, tổng đài điện thoại, các hệ thống tài khoản ngân hàng trực tuyến và trong các trường hợp khác. Ngoài ra, công nghệ nhận dạng người nói cũng được sử dụng rộng rãi trong các ứng dụng trợ lý ảo, giúp người dùng có thể tương tác với các thiết bị thông minh một cách dễ dàng hơn. Nhiều công ty công nghệ lớn đã triển khai các trợ lý ảo sử dụng công nghệ nhận dạng người nói, bao gồm Google Assistant, Amazon Alexa, Apple Siri và Microsoft Cortana. Phần tiếp theo sẽ đánh giá tình hình nghiên cứu thuộc lĩnh vực của đề tài trên khía cạnh dữ liệu và công nghệ.

---

### Về dữ liệu

Một bộ dữ liệu lớn về nhận dạng người nói đóng vai trò rất quan trọng trong việc phát triển các hệ thống nhận dạng người nói hiệu quả. Với một lượng dữ liệu đủ lớn, mô hình nhận dạng người nói có thể học được đa dạng các đặc trưng và biểu diễn giọng nói của nhiều người nói khác nhau. Điều này giúp tăng tính tổng quát và độ chính xác của mô hình, từ đó cải thiện hiệu suất của các ứng dụng sử dụng nhận dạng người nói.

Bên cạnh đó, các thử nghiệm của chúng tôi cho thấy, việt kết hợp dữ liệu tiếng Anh (vốn rất phong phú đa dạng) với lượng dữ liệu tiếng Việt (hạn chế, không đa dạng) cũng cho kết quả khá tốt với một số tập dữ liệu kiểm thử (bảng 1).

  |                                                          |                   | **_Dữ liệu kiểm thử_** | |
  | -------------------------------------------------------- | ------------------| ------------- | -------------- |
  | **_Dữ liệu huấn luyện_**                                 | **VoxCeleb1(TA)** | **VTR67(TV)** | **ZALO20(TV)** |
  | **VoxCeleb2 (TA)**                                       | 1.12              | 5.07          | 11.11          |
  | **VoxCeleb2 (TA)<br>\+ VTR1212 (TV)<br>\+ ZALO380 (TV)** | 8.86              | 1.03          | 6.48           |
  | **VTR1212 (TV)<br>\+ ZALO380 (TV)**                      | 15.51             | 1.26          | 5.56           |

  _Bảng 2: Tỷ lệ nhận diện sai (EER - %) với cặp dữ liệu huấn luyện / kiểm thử khác nhau_    


#### Ngoài nước

Một số bộ dữ liệu tiếng nước ngoài tiêu biểu:

- *VoxCeleb 1 và 2*: Đây là 2 bộ dữ liệu quy mô lớn được sử dụng rất phổ biến trong lĩnh vực nhận dạng người nói, được phát triển bởi các nhà nghiên cứu tại Đại học Oxford. Trong đó, VoxCeleb 1 bao gồm hơn 120.000 đoạn ghi âm giọng nói từ hơn 1.200 người nổi tiếng trên thế giới, bao gồm các diễn viên, nhạc sĩ, vận động viên, chính trị gia, v.v. Bộ dữ liệu này được thu thập từ các video trên YouTube, với các nội dung đa dạng từ các phim ảnh, chương trình truyền hình, phỏng vấn, đến các video diễn thuyết. Trong khi đó, VoxCeleb 2 là phiên bản nâng cấp của VoxCeleb 1, với hơn 1 triệu đoạn ghi âm giọng nói từ khoảng 6.000 người. Bộ dữ liệu này được thu thập từ các video trên YouTube và có tính đa dạng về vùng miền, ngôn ngữ và nghề nghiệp của người nói. VoxCeleb 1 và 2 là hai bộ dữ liệu quan trọng để đào tạo các mô hình nhận dạng người nói trên quy mô lớn và giúp cải thiện hiệu suất của các hệ thống nhận dạng người nói trong các ứng dụng thực tế.
- *CN-Celeb*: Bộ dữ liệu nhận dạng người nói lớn được xây dựng cho tiếng Trung Quốc. CN-Celeb bao gồm hơn 10.000 người nổi tiếng với hơn 1 triệu mẫu âm thanh. Các mẫu âm thanh được thu thập từ nhiều nguồn khác nhau như các cuộc phỏng vấn truyền hình, phim truyền hình, phỏng vấn báo chí, video trực tuyến và các cuộc gọi điện thoại. Các mẫu âm thanh được gán nhãn với thông tin người nói, giới tính, độ tuổi và nghề nghiệp.
- *NIST SRE CTS Superset*: Bộ dữ liệu NIST SRE CTS Superset là một tập hợp các dữ liệu giọng nói thu thập từ các cuộc thi Speaker Recognition Evaluation (SRE) của Viện Tiêu chuẩn và Công nghệ Quốc gia Hoa Kỳ (NIST). Bộ dữ liệu này bao gồm các file âm thanh giọng nói của hơn 4500 người, với tổng cộng hơn 30.000 giờ âm thanh. Các dữ liệu này được thu thập từ nhiều nguồn khác nhau, bao gồm các cuộc gọi điện thoại, phát biểu trên truyền hình, đọc tin tức và đọc sách. Dữ liệu trong bộ dữ liệu này có tính đa dạng về ngôn ngữ, giới tính, tuổi tác và độ phức tạp của nhiễu và kênh truyền. NIST SRE CTS Superset được sử dụng rộng rãi trong các nghiên cứu và ứng dụng trong lĩnh vực nhận dạng người nói, như xây dựng và đánh giá hiệu suất của các thuật toán nhận dạng người nói, phát hiện giọng nói giả và xác thực người dùng.

#### Trong nước

Tại Việt Nam, công nghệ nhận dạng người nói cũng dang nhận được nhiều sự quan tâm và đầu tư phát triển. Tuy nhiên, đến thời điểm vẫn chưa có một bộ dữ liệu lớn về giọng nói tiếng Việt nào. Các nghiên cứu với tiếng Việt trong lĩnh vực này chủ yếu sử dụng các bộ dữ liệu nhỏ được cung cấp bới các cuộc thi về trí tuệ nhân tạo:

- *Zalo AI Challenge 2020*: ZALO-SID-400, được sử dụng trong cuộc thi ZALO AI Challenge 2020, là một tập dữ âm thanh nói gồm 8,7 giờ ghi âm, với tần số lấy mẫu 48000Hz, được ghi âm bởi một nhóm được lựa chọn gồm 400 phát thanh viên sử dụng kịch bản có sẵn. Hầu hết các câu nói có độ dài từ 4 đến 12 giây, trong khi hầu hết các người nói bao gồm từ 15 đến 40 câu nói. Các chủ đề bao gồm tin tức chính trị, đời sống, sức khỏe, thể thao,..
- *VLSP 2021*: VLSP21-SID-1300, được sử dụng trong cuộc thi VLSP 2021, gồm 45 giờ dữ liệu âm thanh từ 1305 người với tần số lấy mẫu là 16000Hz. Nguồn là từ YouTube trộn với bộ ZALO-SID-400.
- *VLSP 2022*: VLSP22-SID-91, được sử dụng trong cuộc thi VLSP 2022, gồm 4 giờ dữ liệu âm thanh từ 91 người nói với tần số lấy mẫu 16000Hz. Nguồn là bộ dữ liệu Mozilla Common Voice.

Nhìn chung, các bộ dữ liệu huấn luyện cho hệ thống nhận dạng người nói vẫn còn khiêm tốn cả về thời lượng lẫn số người tham gia. Chất lượng, môi trường ghi âm vẫn còn không đồng đều ngay trong chính mỗi bộ dữ liệu.

---

### Về công nghệ

Công nghệ xác minh người nói đã trải qua sự tiến bộ đáng kể, đặc biệt là trong lĩnh vực mạng trích xuất vector đặc trưng người nói. Các mạng này đóng vai trò quan trọng trong việc đại diện và phân biệt các đặc điểm giọng nói độc đáo của mỗi người để thực hiện việc xác thực. Bằng cách trích xuất vector đặc trưng người nói, các mạng này tạo ra biểu diễn dữ liệu giọng nói nhỏ gọn và mạnh mẽ, từ đó hỗ trợ hiệu quả cho các hệ thống xác minh người nói. Các mạng trích xuất vector đặc trưng người nói hiện đại sử dụng các kiến trúc học sâu như CNN, RNN và TDNN với cơ chế tích hợp ngữ cảnh mở rộng. Mục tiêu của chúng là chuyển đổi các đoạn giọng nói có độ dài khác nhau thành các vector đặc trưng người nói có số chiều cố định, từ đó ghi lại thông tin cụ thể về người nói trong khi giảm thiểu sự biến đổi do ngôn ngữ và tiếng ồn nền. Tiếp sau đây sẽ là phần trình bày về các công nghệ nhận nhận diện người nói tân tiến nhất.

  |                  |  | Dữ liệu kiểm thử |  |
  | ---------------- | ---------------- | -- |-- |
  | **Mô hình**      | VoxCeleb1-O (TA) | VoxCeleb1-E (TA) | Voxceleb1-H (TA) |
  | XVector          | 1.49             | 1.37             | 2.35             |
  | ResNet           | 1.19             | 1.33             | 2.46             |
  | ECAPA-TDNN       | 0.87             | 1.12             | 2.12             |
  | MFA-Conformer    | 0.64             | \-               | \-               |

* X-Vector : Snyder, D., Garcia-Romero, D., Sell, G., Povey, D., & Khudanpur, S. (2018). X-Vectors: Robust DNN Embeddings for Speaker Recognition. 2018 IEEE International Conference on Acoustics, Speech and Signal Processing (ICASSP), 5329-5333.
* ResNet : Szegedy, C., Ioffe, S., Vanhoucke, V., & Alemi, A.A. (2016). Inception-v4, Inception-ResNet and the Impact of Residual Connections on Learning. ArXiv, abs/1602.07261.
* ECAPA-TDNN : Desplanques, B., Thienpondt, J., & Demuynck, K. (2020). ECAPA-TDNN: Emphasized Channel Attention, Propagation and Aggregation in TDNN Based Speaker Verification. ArXiv, abs/2005.07143.
* MFA-Conformer: Zhang, Y., Lv, Z., Wu, H., Zhang, S., Hu, P., Wu, Z., Lee, H., & Meng, H.M. (2022). MFA-Conformer: Multi-scale Feature Aggregation Conformer for Automatic Speaker Verification. Interspeech.

### X-Vector

X-Vector là một phương pháp quan trọng và hiệu quả trong xác minh người nói. Với kiến trúc mạng nơ-ron sâu của mình, X-Vector trích xuất các vector đặc trưng mạnh mẽ và phân biệt từ tín hiệu giọng nói đầu vào. Nó đóng vai trò quan trọng trong việc xác minh chính xác danh tính người nói bằng cách xây dựng các biểu diễn đáng tin cậy cho từng người nói. X-Vector đã được áp dụng rộng rãi và cho thấy sự tiến bộ đáng kể trong các nhiệm vụ xác minh người nói, nâng cao hiệu suất và độ chính xác của các hệ thống xác minh người nói. Tác động của nó lan rộng trong các lĩnh vực như xác thực cá nhân, giám sát trung tâm cuộc gọi và phân tích giọng nói pháp y. Kiến trúc X-Vector bao gồm một số thành phần quan trọng:

* Mạng Nơ-ron Cấp khung thoại (Frame-Level Neural Network): Mạng nơ-ron cấp khung thoại, thường là mạng nơ-ron trễ thời gian (TDNN) hoặc mạng nơ-ron tích chập (CNN), được sử dụng để mô hình các sự phụ thuộc thời gian trong từng khung thoại đặc trưng âm. Mạng này ghi lại các mẫu và biến thể cục bộ trong tín hiệu giọng nói.
* Xử lý Cửa sổ Ngữ cảnh (Context-Window Processing): Tiếp sau mạng cấp khung thoại thường là một bước xử lý cửa sổ ngữ cảnh. Bước này tổng hợp thông tin từ các khung thoại lân cận để ghi lại thông tin ngữ cảnh rộng hơn. Nó giúp mô hình các phụ thuộc dài hạn trong tín hiệu giọng nói.
* Gom hợp Thống kê (Statistical Pooling): Sau bước xử lý cửa sổ ngữ cảnh, gom hợp thống kê được thực hiện để tóm tắt các biểu diễn cấp khung thoại thành một vector đặc trưng người nói có số chiều cố định. Các kỹ thuật gom hợp thông thường bao gồm gom hợp trung bình và độ lệch chuẩn, ghi lại các thuộc tính phân phối của các biểu diễn cấp khung thoại.
* Phân loại Người nói (Speaker Classification): Các vector đặc trưng người nói sau đó được truyền qua một lớp feed-forward cuối cùng, thực hiện phân loại người nói. Lớp này ánh xạ các vector đặc trưng người nói vào một không gian phân biệt, nơi có thể phân biệt hiệu quả giữa các người nói.

#### ResNet

ResNet (Residual Networker - Mạng Phần Dư) là một kiến trúc học sâu được thiết kế để huấn luyện các mạng neural sâu rất mạnh. Điểm đột phá chính trong ResNet là việc giới thiệu "kết nối tắt" (skip connections) hoặc "kết nối bỏ qua", cho phép đầu ra của một lớp đi qua một hoặc nhiều lớp tiếp theo trước khi được cộng với đầu ra của những lớp đó. Điều này giúp giảm thiểu vấn đề đạo hàm nhỏ dần (vanishing gradient), một vấn đề phổ biến khi huấn luyện các mạng neural sâu, trong đó đạo hàm trở nên ngày càng nhỏ khi truyền ngược qua các lớp, làm cho mạng khó huấn luyện.

Trong lĩnh vực xác minh người nói, ResNet đã chứng minh hiệu quả của mình trong việc trích xuất các đặc trưng đa tỉ lệ (multi-scale features) người nói. Bằng cách tận dụng kết nối tắt, ResNet có thể hiệu quả trong việc nắm bắt và mô hình hóa những đặc điểm giọng nói độc đáo của từng cá nhân, nâng cao khả năng phân biệt của các đặc trưng đã học. Kết nối tắt cho phép ResNet học các ánh xạ phần dư (residual mappings), giúp bảo toàn thông tin quan trọng về người nói trong quá trình huấn luyện mạng sâu.

Res2Net, một phiên bản phát triển từ ResNet, mang đến những cải tiến bằng cách giới thiệu "tỉ lệ dư" (residual scaling) và các nhánh song song trong từng khối tỉ lệ dư. Sự cải tiến này giúp Res2Net nắm bắt thông tin đa tỉ lệ một cách hiệu quả hơn. Trong ngữ cảnh xác minh người nói, Res2Net có thể cải thiện việc trích xuất các đặc trưng đa tỉ lệ người nói bằng cách nắm bắt cả các chi tiết tinh tế và thông tin ngữ nghĩa cấp cao liên quan đến giọng nói của người nói. Ứng dụng của ResNet và Res2Net trong xác minh người nói đã cho thấy kết quả đáng khích lệ về độ chính xác và tính ổn định của việc nhận dạng người nói.

#### ECAPA-TDNN

ECAPA-TDNN (Mạng nơ-ron thời gian TDNN mở rộng với Tập trung Kênh (Channel Attention), Truyền Đạt (Propagation) và Tích hợp Ngữ cảnh (Context Aggregation)) là một kiến trúc mạng nơ-ron tiên tiến được thiết kế cho việc xác minh người nói, nhiệm vụ liên quan đến xác thực cá nhân dựa trên giọng nói. Kiến trúc này được giới thiệu trong bài báo "ECAPA-TDNN: Emphasized Channel Attention, Propagation and Aggregation in TDNN Based Speaker Verification" của tác giả Wang và cộng sự (2020). ECAPA-TDNN mở rộng kiến trúc Mạng nơ-ron Trễ Thời gian (Time Delay Neural Network - TDNN) bằng cách tích hợp một số thành phần quan trọng để nâng cao hiệu suất xác minh người nói.

* Các điểm chính về ECAPA-TDNN bao gồm:Tập trung Kênh (Channel Attention): ECAPA-TDNN sử dụng cơ chế tập trung kênh để nhấn mạnh sự quan trọng của các kênh đặc trưng trong quá trình trích xuất vector đặc trưng người nói. Bằng cách này, ECAPA-TDNN tập trung vào những đặc trưng quan trọng và giúp cải thiện khả năng phân biệt giữa các người nói.
* Truyền Đạt (Propagation): ECAPA-TDNN sử dụng kỹ thuật truyền đạt thông tin từ các lớp trước đến các lớp sau để cải thiện sự lan truyền thông tin và giảm hiện tượng suy biến độ dốc (vanishing gradient). Điều này giúp cải thiện quá trình huấn luyện mạng và tăng cường khả năng học các biểu diễn đặc trưng của người nói.
* Tích hợp Ngữ cảnh (Context Aggregation): ECAPA-TDNN kết hợp thông tin ngữ cảnh từ các khung thoại thời gian trước và sau để tạo ra các biểu diễn ngữ nghĩa về giọng nói. Bằng cách tích hợp ngữ cảnh, ECAPA-TDNN có khả năng nắm bắt thông tin rộng hơn về đặc trưng giọng nói và tăng cường sự phân biệt giữa các người nói.
* Tích hợp khối Res2Net: ECAPA-TDNN cũng bao gồm một khối Res2Net, một thành phần quan trọng được kế thừa từ kiến trúc Res2Net. Khối Res2Net trong ECAPA-TDNN giúp nắm bắt thông tin đa tỉ lệ và cải thiện khả năng biểu diễn đặc trưng. Điều này cho phép ECAPA-TDNN trích xuất các vector đặc trưng người nói mạnh mẽ và đại diện hơn, từ đó cải thiện hiệu suất trong các nhiệm vụ xác minh người nói.

ECAPA-TDNN đạt được những tiến bộ đáng kể trong lĩnh vực xác minh người nói, đặc biệt trong việc trích xuất và nhận dạng các đặc trưng người nói quan trọng. Kiến trúc này đã chứng tỏ khả năng tăng cường hiệu suất và độ chính xác của hệ thống xác minh người nói, đồng thời cung cấp các giải pháp tiên tiến cho các ứng dụng liên quan đến xác thực cá nhân dựa trên giọng nói.

#### MFA Conformer

MFA-Conformer (Multi-scale Feature Aggregation Conformer) là một kiến trúc mới được thiết kế cho việc xác minh người nói tự động. Nó dựa trên Conformer (Convolution-augmented Transformer), một mô hình kết hợp các ưu điểm của Transformer và Convolutional Neural Network (CNN).

Kiến trúc MFA-Conformer bao gồm các thành phần quan trọng:

* Lớp Giảm mẫu (Convolution Subsampling): Lớp này được giới thiệu để giảm chi phí tính toán của mô hình. Giảm mẫu là một kỹ thuật phổ biến trong xử lý tín hiệu và máy học giúp giảm lượng dữ liệu cần xử lý mà vẫn giữ lại thông tin quan trọng.
* Khối Conformer (Conformer Blocks): Conformer gồm các khối Conformer. Những khối này kết hợp Transformer và CNN để ghi lại cả đặc trưng toàn cục và đặc trưng cục bộ của tín hiệu giọng nói. Transformer tốt trong việc mô hình hóa sự phụ thuộc xa trong dữ liệu, trong khi CNN xuất sắc trong việc ghi lại các mẫu cục bộ. Bằng cách kết hợp hai loại mạng này, các khối Conformer có thể hiệu quả mô hình hóa các mẫu phức tạp và biến thể trong tín hiệu giọng nói.
* Tích hợp Đặc trưng Đa tỉ lệ (Multi-scale Feature Aggregation): Sau khi xử lý tín hiệu giọng nói qua các khối Conformer, các bản đồ đặc trưng đầu ra từ tất cả các khối được ghép lại. Bước này tập hợp các biểu diễn đa tỉ lệ của tín hiệu giọng nói, ghi lại thông tin ở các mức độ chi tiết khác nhau. Điều này rất quan trọng trong xác minh người nói, vì các người nói khác nhau có thể có các đặc trưng đặc biệt ở các tỉ lệ khác nhau.
* Lớp tổng hợp (Pooling): Sau khi tích hợp đặc trưng đa tỉ lệ, thực hiện một bước gom hợp cuối cùng để tóm tắt các biểu diễn cấp khung thành một vector nhúng người nói có số chiều cố định. Vector nhúng này là một biểu diễn gọn nhẹ và phân biệt của giọng nói người nói, có thể được sử dụng để xác minh người nói.

Kiến trúc MFA-Conformer được thiết kế để đồng thời hiệu quả và hiệu năng, mang lại hiệu suất ưu việt trong các nhiệm vụ xác minh người nói trong khi vẫn duy trì được chi phí tính toán quản lý. Khả năng ghi lại cả đặc trưng toàn cục và đặc trưng cục bộ của tín hiệu giọng nói giúp MFA-Conformer đặc biệt mạnh mẽ và chính xác trong việc trích xuất các vector nhúng người nói.

---

## Luận giải về việc đặt ra mục tiêu, đối tượng và những nội dung cần nghiên cứu của đề tài

### Luận giải mục tiêu, đối tượng của đề tài

Theo một báo cáo của MarketsandMarkets, thị trường toàn cầu cho nhận dạng người nói được dự báo sẽ tăng từ 1,1 tỷ USD vào năm 2020 lên đến 3,8 tỷ USD vào năm 2025, với tỷ suất tăng trưởng hàng năm trung bình (CAGR) là 22,6%. Sự tăng trưởng này được thúc đẩy bởi nhu cầu ngày càng tăng về các hệ thống xác thực và nhận diện an toàn, đặc biệt là trong lĩnh vực tài chính và ngân hàng.

Hơn nữa, một cuộc khảo sát được thực hiện bởi Opus Research vào năm 2020 cho thấy 80% các doanh nghiệp dự định triển khai các công nghệ nhận diện giọng nói trong vòng hai năm tới. Điều này cho thấy một xu hướng mạnh mẽ hướng đến việc áp dụng các hệ thống nhận dạng người nói trong môi trường thương mại.

<figure>
<img src="Voice-Biometric-Market.png"
alt="Voice-Biometric-Market.png" />
<figcaption>Giá trị thị trường sản phẩm xác thực người nói
(2018-2030)</figcaption>
</figure>

Hiện tại chưa có số liệu chính thức về kích thước thị trường của các dịch vụ liên quan đến nhận dạng / xác thực giọng nói tại Việt Nam. Tuy nhiên, theo xu hướng toàn cầu, việc áp dụng các công nghệ nhận dạng người nói đang ngày càng phổ biến và được sử dụng rộng rãi trong nhiều lĩnh vực, từ tài chính, bảo mật cho đến truyền thông và giáo dục. Việc sử dụng các hệ thống nhận dạng người nói cũng sẽ giúp tăng tính bảo mật và tiện lợi cho người dùng, đồng thời giảm thiểu rủi ro gian lận thông tin và các hoạt động giả mạo.

Các nghiên cứu chỉ ra rằng chất lượng hệ thống nhận dạng người tỷ lệ thuận với số lượng người nói, thời lượng và chất lượng của bộ dữ liệu huấn luyện. Với hai ngôn ngữ phổ biến như tiếng Anh và tiếng Trung, các bộ dữ liệu nguồn mở là khá dồi dào, đa dạng và chất lượng. Tuy nhiên, đối với tiếng Việt, lượng dữ liệu đạt yêu cầu như vậy là rất hạn chế.

Trước thực tế đó, mục tiêu của đề tài này là xây dựng một bộ dữ liệu nhận dạng người nói tiếng Việt có quy mô lớn, đa dạng về các loại giọng, và đi kèm chất lượng cao. Bộ dữ liệu mới sẽ đáp ứng nhu cầu cấp bách về nghiên cứu và ứng dụng các hệ thống nhận dạng giọng nói trong thương mại.

### Những nội dung cần nghiên cứu:

Các nội dung cần nghiên cứu bao gồm:

- Xây dựng bộ dữ liệu huấn luyện.
- Xây dựng mô hình học sâu.

---

#### Xây dựng bộ dữ liệu huấn luyện

##### Mô tả

Bộ dữ liệu bao gồm các bản ghi âm của nhiều người nói, với mỗi người nói được đại diện bởi một tập hợp các bản ghi âm. Bộ dữ liệu có quy mô lớn (5000 người, 20+ phút/người); có tính đa dạng về vùng miền, tầng lớp, trình độ, độ tuổi, giới tính, phong cách nói của người nói; bản ghi được phát âm rõ ràng và tự nhiên và thu âm trong điều kiện giống nhau. Quá trình thu âm cũng cần đảm bảo đảm bảo bảo vệ quyền riêng tư của người tham gia.

##### Phương pháp xây dựng

Một bộ dữ liệu huấn luyện xác thực người nói thường được xây dựng bằng cách thu thập các mẫu giọng nói từ một số lượng lớn người nói. Quá trình xây dựng bộ dữ liệu này thường bao gồm một số bước sau:

- *Lựa chọn người nói*: Quá trình lựa chọn người nói là một trong những bước quan trọng nhất trong việc xây dựng bộ dữ liệu huấn luyện xác thực người nói. Để đảm bảo tính đại diện và đa dạng của bộ dữ liệu, ta cần lựa chọn một tập hợp các người nói đa dạng, bao gồm cả nam và nữ, các độ tuổi khác nhau, đại diện cho nhiều giọng nói và phương ngữ khác nhau.
- *Lựa chọn kịch bản*: Kịch bản cần được thiết kế cẩn thận để mô phỏng luồng tự nhiên của cuộc trò chuyện, bao gồm các đoạn hội thoại thực tế và một loạt các chủ đề trò chuyện.
- *Ghi âm*: Sau khi lựa chọn được tập hợp các người nói, các người nói sẽ được yêu cầu ghi âm các mẫu giọng nói. Để đảm bảo chất lượng của dữ liệu thu thập được, cần sử dụng các thiết bị ghi âm chất lượng cao, đồng thời tạo điều kiện thuận lợi cho các người nói để giảm thiểu tiếng ồn, nhiễu và các yếu tố khác ảnh hưởng đến chất lượng thu âm.
- *Gán nhãn*: Sau khi thu âm các mẫu giọng nói, các mẫu này sẽ được gán nhãn theo thông tin về danh tính, tuổi, giới tính, ngôn ngữ, giọng nói của người nói. Việc gán nhãn chính xác và đầy đủ các thông tin này là rất quan trọng để tạo ra một bộ dữ liệu đại diện và chất lượng cao, đồng thời giúp cho việc xác định các người nói trong bộ dữ liệu được chính xác.

Để xác định số lượng dữ liệu cần ghi âm cho mỗi giọng do hiện tại chưa có một nghiên cứu thống kê cụ thể nào về mối liên hệ giữa chất lượng hệ thống nhận dạng giọng nói với dữ liệu huấn luyện đối với tiếng Việt được công bố. Bởi vậy đề xuất trong đề tài được dựa theo các hệ thống nhận dạng người nói cho tiếng Anh.

Dưới đây là biểu đồ tương quan giữa số giờ dữ liệu huấn luyện và số người với chất lượng của của hệ thống nhận dạng giọng nói tiếng Việt nội bộ Viettel:

*(thực nghiệm để thêm đồ thị)*

---

#### Xây dựng mô hình học sâu

##### Mô tả

Nghiên cứu nâng cao chất lượng hệ thống nhận dạng người nói thông qua cải thiện mô hình mạng học sâu.

##### Phương pháp xây dựng

Các hướng phát triển trong xây dựng mô hình học sâu cho bài toán nhận dạng người nói:

* Nghiên cứu chiến lược kết hợp tất cả các điểm mạnh các kiến trúc học sâu tiên tiến bao gồm XVector, ResNet, Res2Net, ECAPA-TDNN và MFA-Conformer.
* Nghiên cứu và triển khai các kỹ thuật tiền xử lý và hậu xử lý tiên tiến để tăng cường hiệu suất của mô hình.
* Nghiên cứu học máy chuyển giao (transfer learning) để kết hợp nguồn dữ liệu tiếng nước ngoài vô cùng phong phú với lượng dữ liệu tiếng Việt hạn chế để cải thiện độ chính xác và hiệu quả của hệ thống xác minh người nói.
* Nghiên cứu học máy thích ứng miền (domain adapation) để nhanh chóng điều chỉnh model khi cần bài toán mới (vd: sử dụng dữ liệu ghi âm phổ rộng (>16kHz) cho bài toán gọi thoại (8kHz); hay đáp ứng model được huấn luyện với dữ liệu ghi âm nói chuyện phiếm cho môi trường ghi âm khẩu lệnh có sẵn;...)
* Áp dụng các kỹ thuật tăng cường dữ liệu, chẳng hạn như biến đổi tốc độ và tần số, mô phỏng đáp ứng tiếng phòng, biến đổi VTLP, tiêm nhiễu, và các mô hình sinh như Variational Autoencoders (VAEs) hoặc Generative Adversarial Networks (GANs), để tăng cường và đa dạng hóa dữ liệu huấn luyện. Kỹ thuật học không giám sát (unsupervised learning) cũng có thể được sử dụng để tận dụng nguồn dữ liệu không nhãn vô cùng lớn và sau cùng là quay trở lại gán nhãn cho dữ liệu mới.
* Sử dụng các kỹ thuật chuẩn hóa và kết hợp điểm số (scoring), bao gồm T-norm, Z-norm, Adaptive Symmetric Score Normalization (AS-norm), cùng với các phương pháp kết hợp tuyến tính, phi tuyến tính để tối ưu hiệu suất của mô hình trong các nhiệm vụ xác minh người nói.
* Khám phá các kỹ thuật học meta, chẳng hạn như học sắp xếp thứ tự (ordinal regression learning) hoặc học kết hợp (ensemble learning), để có chiến lược huấn luyện kết hợp hiệu quả từ dữ liệu có sẵn.
* Sử dụng các kỹ thuật nén mô hình, như pruning, quantization và knowledge distillation, để giảm yêu cầu tính toán và bộ nhớ, làm cho hệ thống phù hợp với các ứng dụng thời gian thực và các thiết bị có tài nguyên hạn chế.
* Liên tục đánh giá và điều chỉnh mô hình/kiến trúc bằng cách sử dụng các chỉ số hiệu suất để đảm bảo cải thiện liên tục trong hệ thống xác minh người nói.


# Sản phẩm KHCN chính của đề tài và yêu cầu chất lượng cần đạt

EER (Equal Error Rate) là chỉ số đánh giá phổ biến nhất cho hệ thống xác thực người nói. Để tính EER, ta cần vẽ đường cong DET (Detection Error Tradeoff) bằng cách sử dụng hai thông số FAR (False Accept Rate) và FRR (False Reject Rate). FAR là tỷ lệ số lần chấp nhận sai đối với số lần xác thực đối với các người không có quyền truy cập, trong khi FRR là tỷ lệ số lần từ chối sai đối với số lần xác thực đối với các người có quyền truy cập. Đường cong DET có thể được vẽ bằng cách biểu diễn FAR và FRR trên cùng một đồ thị. EER được tính toán bằng cách tìm điểm trên đường cong DET mà FAR và FRR bằng nhau.

Dưới đây là chỉ tiêu kỹ thuật sản phẩm và thông tin so sánh với sản phẩm tương đương:

*(thực nghiệm để thêm bảng)*
